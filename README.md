# Car Sales
# Design Data Intensive Systems - Section 02
# Project Group - 2A
# Project developer pairs
  ## Pair 2-1:
  
 - Abhilash Pochampally

 - Rahul Reddy Lankala

 ##Pair 2-2:
 
 - Lakshman Pavan Kumar Vudutha

 - Sai Teja Jonnalagadda

#Links 
 - Clickable link for repo: https://bitbucket.org/s530664/carsales/overview 


 - Clickable link for Issue tracker: https://bitbucket.org/s530664/carsales/issues?status=new&status=open 
 
#Introduction:
  Here we are using a dataset of Spanish used cars, contains almost 100000 online used cars for sale. By using a data set we are going to find the best car for the better price based on the factors like model of the car, no of months old, no of kilometers, how many owners and so on. Based on these factors the user can select the best car of his/her requirements.
#Data Source:
  Our Data source contains the information about used cars in Spain. The size of the data source is 2 MB. It contains the almost 100000 values. The extension of our dataset is .csv. It is a structured dataset. The dataset includes the information about car like Id, make, model, version, months_old, power, sale_type, num_owners, gear_type, fuel_type, kms, price.
#Data Source Link:
 Link: https://www.kaggle.com/harturo123/online-adds-of-used-cars/data 

#The Challenge:
##Volume:
  In our data set, we are analyzing about one-lakh records. Therefore, we have a challenge to analyze large volume data set
##Variety:
  There are many different types of make and models of cars. Therefore, we have variety of data analyzing problem
##Velocity:
  We have many records so we have to analyze our records very fast.

##Veracity:
  Our data set contains accurate data for customer satisfaction. 
##Value:
  Useful to each one for knowing about the car sales in Spain.
#Big Data Questions:
 1. Which make has the highest number of cars for sale?
   
    As we, all know that not all car companies can be a great choice in a Particular place so it would be easy to know the best car by narrowing down the search by grouping the companies which made the highest sales and then searching for a car in that order.
   
     - Mapper1Output
     ![Mapperoutput](./images/mapper1output.png)
     
     - Reducer1Output
     ![Reduceroutput](./images/reducer1output.png)


	
 2. Among all sales how many are gasoline, diesel, and hybrid?
    
    Choosing the type of fuel for the car also matters a lot, because the type of fuel decides the life of the car.

	- Mapper2Output
      ![Mapperoutput](./images/mapper2Output.png)

     - Reducer2Output
     ![Reduceroutput](./images/reducer2Output.png)
		
	
 3. The cars below price 70000 and used not more than 10000 kilometers?
    
    Another main aspect of selecting a used car depends on number of miles and the car cost. So if we have a price range and Maximum number of miles then the search would be lot greater.
    

     - Mapper3Output
      ![Mapperoutput](./images/mapper3output.PNG)

     - Reducer3Output
     ![Reduceroutput](./images/reducer3output.png)

	

 4. Cars having power greater than 300 and used less than 24 months?
    
    Some people have a great interest in the power of the car, mainly teenagers. So having larger power has few advantages. But very old cars having a good power does not make any sense because it cannot perform that well. So we decided to narrow down our search for cars with power greater than 300 and cars that are used for less than 24 months. 
     - Mapper4Output: 
     ![Mapperoutput](./images/mapper4output.png)

     - Reducer4Output
     ![Reduceroutput](./images/reducer4output.png)



# Big Data Solutions:
## Mapper input:
  Question3:
  62391;Bentley;Continental;Gtc Speed *Modell 2016* Naim Soundsystem;11;467;used;;automatic gasoline;13000;212500
  
## Mapper output/Reducer Input:

  62391,Bentley,Continental,467,11
  ### Question3:
   9900,284900
  
## Reducer output:
  62391,Bentley,Continental
  ### Question3:
      9999	13
      Total number of cars having price less than 70,000 and driven less than 10,000 miles is : 13140
  
## Language:
  Python

##Commands for execution:

- mapper execution: python mappername.py(name of the mapper with .py extension)

- reducer execution: python reducername.py(name of the reducer with .py extension)

## Charts:
  Bar graph for number of months old < 24 and power>300 to number of cars 
  
## Number of cars with age < 24 and power >  300 
  ![Bargarph](/images/Make.PNG)
  ![Bargarph](/images/Chart_monthsandpower.PNG)

## Number of cars with different fuel types like Disel,petrol,Hybrid
  ![Bargarph](/images/FuelBarGraph.png)
	
	
   Histogram graph for cars kms ranging 0 to 10,000 and price less than 70,000
## Number of cars with miles 0 to 10,000
  ![Histogram](/images/cars_kms_Histogram.PNG)
  


  
  


   
   
  


  
 



