s = open("sort.txt","r") # open file, read-only
r = open("reduce.txt", "w")# open file, write

thisKey = ""
thisValue = 0

for line in s:
  data = line.strip().split('\t')
  id, num = data

  if id != thisKey:
    if thisKey:
        # output the last key value pair result
      r.write(thisKey + '\t' + str(thisValue)+'\n')
# start over when changing keys
    thisKey = id 
    thisValue = 0

# apply the aggregation function
  thisValue += int(num)


# output the final entry when done
r.write(thisKey + '\t' + str(thisValue)+'\n')

s.close()
r.close()